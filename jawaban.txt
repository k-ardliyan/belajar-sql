-- [SOAL 1]
-- MEMBUAT DATABASE myshop
CREATE DATABASE myshop;

-- Masuk ke db myshop
USE myshop;

-- [SOAL 2]
-- MEMBUAT TABLE users
CREATE TABLE users (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    PRIMARY KEY(id)
) ENGINE = InnoDB;

-- MEMBUAT TABLE categories
CREATE TABLE categories (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	PRIMARY KEY(id)
)ENGINE = InnoDB;

-- MEMBUAT TABLE items 
CREATE TABLE items (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255),
    description VARCHAR(255),
    price INT NOT NULL,
    stock INT NOT NULL,
    category_id INT NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT fk_items_category FOREIGN KEY (category_id) REFERENCES categories (id)
) ENGINE = InnoDB;

-- CHECK TABLES
SHOW TABLES;

-- [SOAL 3]
-- MEMASUKKAN DATA KE TABLE

INSERT INTO users(name, email, password)
VALUES ('John Doe', 'john@doe.com', 'john123'),
	   ('Jane Doe', 'jane@doe.com', 'jenita123');
	   
INSERT INTO categories(name)
VALUES ('gadget'),('cloth'),('men'),('women'),('branded');

INSERT INTO items(name, description, price, stock, category_id)
VALUES  ('Sumsang b50', 'hape keren dari merek sumsang', 4000000, 100, 1),
		('Uniklooh', 'baju keren dari brand ternama', 500000, 50, 2),
		('IMHO Watch', 'jam tangan anak yang jujur banget', 2000000, 10, 1);
		
-- [SOAL 4]
-- MENGAMBIL DATA DARI TABEL

-- a. Data users tanpa password
SELECT id, name, email FROM users;

-- b. Data items
SELECT * FROM items
WHERE price > 1000000;

SELECT * FROM items
WHERE name LIKE '%uniklo%';

-- c. Data items join with category
SELECT i.name, description, price, stock, category_id, c.name as kategori
FROM items as i
JOIN categories as c ON (c.id = i.category_id);

-- [SOAL 5]
-- MENGUBAH DATA DATABASE

UPDATE items
SET price = 2500000
WHERE name = 'Sumsang b50';